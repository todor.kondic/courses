# Getting Started

* Fork and then clone the tutorial repository
  <a href="https://github.com/LCSB-BioCore/advanced-git-practice">https://github.com/LCSB-BioCore/advanced-git-practice</a>

    ```bash
    $ git clone git@github.com:<first.last>/advanced-git-practice.git
    ```

* Add a remote `upstream`
    ```bash
    $ cd advanced-git-practice
    # add upstream URL
    $ git remote add upstream git@github.com:LCSB-BioCore/advanced-git-practice.git
    $ git fetch upstream
    ```
* Check the remotes with:
    ```bash
    $ git remote -v
    ```

* Create the `develop` branch and your own branch `myBranch` based on the `develop` branch from `upstream` using the `-b` flag
    ```bash
    $ git checkout -b develop upstream/develop
    $ git checkout -b myBranch
    ```