# Ignore files

In the file `.gitignore`, all untracked files that should be ignored by `git` are listed:
```
.DS_Store
data
*.csv
```

If you want to exclude certain files from this list:
```
!*.yml
config/*.yml
```
This tracks changes in all yml file, but not in the `config` folder.

Examples: www.gitignore.io