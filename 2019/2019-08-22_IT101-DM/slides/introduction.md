# Introduction
<div  class="fragment" style="position:absolute">
<img   height="450px" src="slides/img/wordcloud.png"><br>

## Learning objectives

  * How to manage your data
  * How to look and analyze your data
  * Solving issues with computers
  * Reproduciblity in the research data life cycle

</div>
<div  class="fragment" style="position:relative;left:50%; width:40%">
<div >
<center>
<img   height="405px" src="slides/img/rudi_balling.jpg"><br>
Prof. Dr. Rudi Balling, director
</center>
</div>

## Pertains to practically all people at LCSB
   * Scientists
   * PhD candidates
   * Technicians
   * Administrators
</div>
