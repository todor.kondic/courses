# Installation of `git`

<img src="slides/img/github_app.png" class="as-is" height="200" />

**macOS**

Install *Xcode Command Line Tools*


**Windows**

Install Git Bash: <br>`https://git-scm.com/download/win`


**Linux (Ubuntu)**

```bash
$ sudo apt-get install git-all
```



# How to get started?

**macOS**

Start the `Terminal` or `iTerm`.


**Windows**

Start `GUI Bash`.


**Linux (Ubuntu)**

Start the `Terminal` or `Terminator`.


**Is `git` properly installed?**

```bash
$ git --version
# git version 2.10.0
```