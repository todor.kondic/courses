# The editor(s)

Recommended editors:

- **Visual Studio Code** (https://code.visualstudio.com)
- **Atom** (https://atom.io)

*Note*: Other editors such as IntelliJ IDEA (https://www.jetbrains.com/idea) or Sublime Text (https://www.sublimetext.com) can, of course, also be used.

<img src="slides/img/icon-live-demo.png" height="100px">