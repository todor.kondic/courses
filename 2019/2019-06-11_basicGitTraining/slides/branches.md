# Development scheme


Generally, in a repository, there are guidelines for contributing.

<div class="fragment">

A common development scheme is dual with a:

- **development** version of the code on `develop`
- **stable** version of the code on `master`


A **version** of the code is referred to as a **branch**.

<div class="fragment">

<img src="slides/img/icon-live-demo.png" height="100px">


<font color="red">In the practice repository, the development branch is called `develop`!</font>

<div class="fragment">

![bulb](slides/img/bulb.png) Use this dual development scheme for your own repositories!



# Branches

A **version** of the code (i.e., a **branch**) is made up of a sequence of code changes.

<div class="fragment">

These individual code changes are called **commits**.


For instance, the `master` and `develop` branches can be represented as a timeline:
<img src="slides/img/branch-master.png" class="branch-master" height="500em"/>



# Switch between branches

List all branches of the repository with
```bash
$ git branch -a
```

Exit by typing `q`. The branch with the * is the current branch.

<div class="fragment">

Checkout another branch
```bash
$ git checkout <branchName>
```

<div class="fragment">

You can switch to the `develop` branch with
```bash
$ git checkout develop
```
If the local branch does not exist but the remote does, it is created automatically.

<div class="fragment">

<img src="slides/img/icon-live-demo.png" height="100px">



# Create your own version

Assume that you want to work on a file:

<div class="fragment">

<font color="red">Create a new **branch**!</font>

```bash
$ git checkout -b myBranch
```
The `-b` flag creates the branch. Locally, you have your own version now:
<img src="slides/img/branch-create.png" class="branch-create" height="500em"/>




Push your version to your fork:
```bash
$ git push origin myBranch
```


<div class="fragment">

<img src="slides/img/icon-live-demo.png" height="100px">
