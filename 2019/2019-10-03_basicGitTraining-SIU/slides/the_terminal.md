# Note on Microsoft Azure Devops

This course aims at the basics of using `git`.

In order to understand the concepts of `git`, the terminal will be used instead of any GUI.
The same terminology applies.

More info on Azure Devops & Git: https://docs.microsoft.com/en-us/azure/devops/repos/git/



# First steps in the terminal

Starting the terminal presents itself with a line where you can enter a command
```bash
cesar@myComputer>
```

Often written, for covenience, as
```bash
$
```

When you open your terminal (shell), you are located
in your home directory (unless otherwise configured), denoted as `~/`.



# Essential Linux commands (i)

List the contents of a directory
```bash
$ ls #-lash
```

Create a directory
```bash
$ mkdir myNewDirectory
```

Change the directory to a specific folder
```bash
$ cd myNewDirectory
```

Change the directory 1 level and 2 levels up
```bash
$ cd ..
# 1 level up

$ cd ../..
# 2 levels up
```



# Essential Linux commands (ii)

Move a file or a directory
```bash
$ mv myFile.m myNewDirectory/.
```


Rename a file or a directory
```bash
$ mv myFile.m myNewFile.m
$ mv myDirectory myNewDirectory
```