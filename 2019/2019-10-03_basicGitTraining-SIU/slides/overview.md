# Overview

1. What is `git`? What is the use of `git`?
2. GitHub and GitLab
3. The terminal
4. What is a fork?
5. What are branches?
6. The 5 essential commands (`pull` / `status` / `add` / `commit` / `push`)
7. What are merge/pull requests?
8. How do I synchronize my fork?
9. Best practices
