# Cherry-picking

* Cherry-picking allows to pick one (or more) specific commits from a list of commits.

* Only the chosen commit(s) are picked, not everything up to that commit.

<div style="top: 8em; left: 25%; position: absolute;">
    <img src="slides/img/cherryPick.png" height=500px>
</div>



# Example (1)

* Create and commit two files in the `develop ` branch
```bash
$ git checkout develop
$ echo "# Venue details" > location.md
$ # add and commit the file location.md
$ echo "# Speakers" > speakers.md
$ # add and commit the file speakers.md
```



# Example (2)

* Check the `log` and note down the `SHA1` of the commits you want to cherry-pick. Then:
```bash
$ git checkout myBranch
$ git cherry-pick <SHA1>
```
* Check the log again and see if the changes were applied correctly. Note the new SHA1!
```bash
$ git show <newSHA1>
```
* Repeat for the second commit

* Push the changes to `myBranch`
```bash
$ git push origin myBranch
```

* Note that the `-f` flag is not needed to force push (no history has been rewritten)



# Partial chery-picking

* Partial cherry-picking allows you to unpack the changes from a commit.

* Imagine you committed many files, and you want to remove certain files.

* In practice:

    - You commited all files, and you realize that there is your data inside!

    - You have committed accidentally sensitive data, such as your password

    - You committed hidden files, for instance `.DS_Store` files

    - ...




# Example (1)

* Hard reset the `myBranch` branch:
```bash
$ git checkout myBranch
$ git reset --hard HEAD~2 # do not preserve files
```

* Reset the `develop` branch:
```bash
$ git checkout develop
$ git reset HEAD~2 # preserve files
```
* Add the `location.md` and the `speakers.md` files as 1 commit:
```bash
$ git add location.md speakers.md
$ git commit -m "add location and speakers files"
```



# Example (2)

Cherry-pick the commit from `develop` over to `myBranch`:

```bash
$ git checkout myBranch
$ git cherry-pick -n <SHA1>
$ git status
```
Now, remove the file `location.md`:
```bash
$ git restore --staged location.md # old version of git: $ git reset HEAD location.md
$ rm location.md
```
Commit the changes:
```bash
$ git commit -m "add speakers file"
```
