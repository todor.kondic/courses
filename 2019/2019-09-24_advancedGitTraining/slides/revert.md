# Reverting a commit

* Enables the deletion of committed commits by reverting the changes.

* A trace is kept in history of the original commit and the reverted one.



# Example:

* On your branch, create and commit a file:
```bash
$ echo "# Grades for Firstname Lastname" > grades.md
$ git add grades.md
$ git commit -m "File with grades for Firstname Lastname"
```

* Note down the `SHA1` by checking the `log`

* Use the `git revert` command to undo that commit:
```bash
$ git revert <SHA1>
```
* This will open a dialog (`vim`-like editor). Exit with `:wq`