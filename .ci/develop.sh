#!/bin/bash

if  [[ $1 = "-i" ]]; then
    echo "Install from scratch"

    npm install -g npm@latest grunt-cli generator-reveal
    npm install

    git submodule update --init
else
    echo "Install already performed. Only watching."
fi

grunt server

