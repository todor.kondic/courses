# Thank you.<sup> </sup>
<center><img src="slides/img/r3-training-logo.png" height="200px"></center>
<br>
<br>
<br>
<br>
<center>
Contact us if you need help:

<a href="mailto:r3lab.core@uni.lu">r3lab.core@uni.lu</a>
</center>

Links:  
HowTo cards: https://howto.lcsb.uni.lu/  
HPC: https://hpc.uni.lu/  
LCSB GitLab: https://git-r3lab.uni.lu/  
Service Portal: https://service.uni.lu/sp
